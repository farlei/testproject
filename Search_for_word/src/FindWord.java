
import java.util.Scanner;
public class FindWord {

	public static void main(String[] args) {
		String text = ("“I felt like it wasn’t enough. It seemed like nothing. I was disappointed,” I said.\n" + 
				"“And that is how most employees feel when they look at their paychecks—especially after all the tax and other deductions are taken out. At least you got 100 percent.”\n" + 
				"“You mean most workers don’t get paid everything?” I asked with amazement.\n" + 
				"“Heavens no!” said rich dad. “The government always takes its share first.”\n" + 
				"“How do they do that?” I asked.\n" + 
				"“Taxes,” said rich dad. “You’re taxed when you earn. You’re taxed when you spend. You’re taxed when you save. You’re taxed when you die.”\n" + 
				"“Why do people let the government do that to them?”\n" + 
				"“The rich don’t,” said rich dad with a smile. “The poor and the middle class do. I’ll bet you that I earn more than your dad, yet he pays more in taxes.”\n" + 
				"“How can that be?” I asked. At my age, that made no sense to me. “Why would someone let the government do that to them?”\n" + 
				"Rich dad rocked slowly and silently in his chair, just looking at me. “Ready to learn?” he asked.\n" + 
				"I nodded my head slowly.\n" + 
				"“As I said, there is a lot to learn. Learning how to have money work\n" + 
				"for you is a lifetime study. Most people go to college for four years,\n" + 
				"and their education ends. I already know that my study of money will continue over my lifetime, simply because the more I find out, the more I find out I need to know. Most people never study the subject. They go to work, get their paycheck, balance their checkbooks, and that’s it. Then they wonder why they have money problems. They think that more money will solve the problem and don’t realize that it’s their lack of financial education that is the problem.”");
		Scanner scan = new Scanner(text);
		System.out.println(text);
		
		Scanner scan2 = new Scanner(System.in);
		System.out.println();
		System.out.println("Enter search word: ");
		String searchWord = scan2.next();
		int count = 0;
		
		while(scan.hasNext() ) {
			if (scan.next().equalsIgnoreCase(searchWord)) {
				count++;
			}
		}
		scan.close();
		scan2.close();
		System.out.println("the word: \""+searchWord+ "\" "+"repeated "+count+" time/times");
		
		}

	}

